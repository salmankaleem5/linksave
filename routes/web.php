<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// php artisan route:list
Auth::routes();

// https://laravel.com/docs/5.6/controllers#resource-controllers
Route::resource('webpages', 'WebpageController');

Route::get('/', 'WebpageController@index')->name('home');

Route::resource('user.userWebpage', 'UserWebpageController')->only([
    'store', 'destroy'
]);

Route::resource('collection.user', 'CollectionUserController')->only([
    'store', 'update', 'destroy'
]);
Route::post('collection/{collection}/user/bulk-delete', 'CollectionUserController@bulkDestroy');
Route::post('collection/{collection}/user/bulk-update', 'CollectionUserController@bulkUpdate');

Route::resource('collection', 'CollectionController');

Route::resource('collections.userWebpage', 'CollectionUserWebpageController')->only([
    'store', 'destroy'
]);
