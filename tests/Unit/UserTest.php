<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function test_auth_user_can_add_webpage(){
        // Sign in as user
        // Create a webpage
        // User saves webpage
        // Web-page saved in database and user can retrieve web-page
        $user = factory('App\User')->create();
        $this->actingAs( $user );

        $webpage = factory('App\Webpage')->make();

        $user->addWebpage($webpage);

        $this->assertDatabaseHas('webpages', [
            'url' => $webpage->url
        ]);

        $this->assertTrue(
            $user->webpages()->where('url', $webpage->url)->exists()
        );
    }
}
