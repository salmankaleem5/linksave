<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WebpageTest extends TestCase
{
    use RefreshDatabase;

    public function test_auth_user_can_view_saved_webpages(){
      // Sign in as user
      $user = factory('App\User')->create();
      $this->actingAs( $user );

      // Save webpage to user
      $webpage = factory('App\Webpage')->create();
      $user->addWebpage($webpage);

      // Check if webpage is visible on user's homepage
      $response = $this->get('/');
      $response->assertSee($webpage->title);
    }
}
