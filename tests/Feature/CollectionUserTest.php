<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CollectionUserTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_add_other_users_to_collection(){
        // Collection owner can give roles to other users in the owner's collection
        // Authenticated user
        // Creates collection
        // Adds user
        $this->actingAs( factory('App\User')->create() );

        $userToAdd = factory('App\User')->create();
        $roleToAssign = factory('App\Role')->create();

        $collection = factory('App\Collection')->create([
            'user_id' => auth()->id()
        ]);

        $this->post("/collection/{$collection->id}/user", [
            'roleId' => $roleToAssign->id,
            'email' => $userToAdd->email
        ]);

        $this->assertDatabaseHas('collection_user_role', [
            'collection_id' => $collection->id,
            'user_id' => $userToAdd->id,
            'role_id' => $roleToAssign->id
        ]);
    }
}
