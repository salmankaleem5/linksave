<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SaveWebpagesFeatureTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_save_pages_on_site(){
        // Authenticated user makes request
        // to save web page
        // User can view web page on site
        $user = factory('App\User')->create();
        $this->actingAs( $user );

        $webpage = factory('App\Webpage')->make();

        $this->post("/user/{$user->id}/userWebpage", $webpage->toArray());

        $this->get('/')
            ->assertSee( $webpage->title );
    }
}
