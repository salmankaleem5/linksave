@extends('layouts.app')

@section('content')
  <home-component :initial-user-webpages="{{ $userWebpages }}" :user-id="{{ Auth::user()->id }}" inline-template>
    <div class="container">
      @if (session('status'))
          <div class="alert alert-success">
              {{ session('status') }}
          </div>
      @endif
      <div class="row">
        <div class="col-12 col-md-8">
          <webpage-form-component @update="updateWebpage" class="mb-3"></webpage-form-component>
          <div v-for="userWebpage in userWebpages" class="row mb-4">
            <div class="col-12">
              <webpage-listitem-component :user-webpage="userWebpage" @destroy="deleteWebpage"></webpage-listitem-component>
            </div>
          </div>
        </div>
        <div class="col-12 col-md-4">
          <p>My Collections:</p>
          <ul>
            @foreach( $collections as $collection )
              <li>
                <a href="{{ $collection->path() }}">{{ $collection->title }}</a>
              </li>
            @endforeach
          </ul>
          <p>Create new collection</p>
          @include('partials.create-form')
        </div>
      </div>
    </div>
  </home-component>
@endsection
