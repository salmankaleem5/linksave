<div>
    @if ( $errors->has('title') || $errors->has('description') )
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('collection.store') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="collection-title-input">Title</label>
            <input type="text" name="title" id="collection-title-input" class="form-control">
        </div>
        <div class="form-group">
            <label for="collection-desc-input">Description</label>
            <input type="text" name="description" id="collection-desc-input" class="form-control">
        </div>
        <button class="btn btn-primary">Create</button>
    </form>
</div>
