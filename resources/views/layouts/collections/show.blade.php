@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1>{{ $collection->title }}</h1>
            <p>{{ $collection->description }}</p>
        </div>
    </div>

    <collection-component :initial-user-webpages="{{ $collectionUserWebpages }}" :collection-id="{{ $collection->id }}" inline-template>
        <div class="row">
            <div class="col-12 col-md-8">
                <div v-for="userWebpage in userWebpages" class="row-mb-4">
                    <div class="col-12">
                        {{-- @TODO: remove the 'remove' link in webpage-listitem for non-authorized users --}}
                        <webpage-listitem-component :user-webpage="userWebpage" :button-text="'Remove'"  @destroy="remove" class="mb-3"></webpage-listitem-component>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4">
                @can('update', $collection)
                    <webpage-select-form-component :pages-not-in-collection="{{ $userWebpagesNotInCollection }}" @update="updateCollectionPages"></webpage-select-form-component>
                @endcan
                @can('delete', $collection)
                    <br>
                    <form action="{{ route('collection.destroy', ['collection' => $collection]) }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button class="btn btn-danger">Delete this collection</button>
                    </form>
                @endcan
                @can('authorize-user', $collection)
                    <br>
                    <button class="btn btn-secondary" type="button" data-toggle="modal" data-target="#permissionsModal">Manage Permissions</button>

                    <permissions-modal-component :collection-id="{{ $collection->id }}" :available-roles="{{ $availableRoles }}" :initial-collection-users="{{ $collectionUsers }}" :is-collection-public="{{ json_encode($collection->is_public) }}"></permissions-modal-component>
                @endcan
            </div>
        </div>
    </collection-component>
</div>
@endsection
