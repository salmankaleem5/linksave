
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('home-component', require('./components/HomeComponent.vue'));
Vue.component('webpage-form-component', require('./components/WebpageFormComponent.vue'));
Vue.component('webpage-listitem-component', require('./components/WebpageListItemComponent.vue'));
Vue.component('collection-component', require('./components/CollectionComponent.vue'));
Vue.component('webpage-select-form-component', require('./components/WebpageSelectFormComponent.vue'));
Vue.component('permissions-modal-component', require('./components/PermissionsModalComponent.vue'));
const app = new Vue({
    el: '#app'
});
