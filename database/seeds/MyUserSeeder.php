<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class MyUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class)->create([
            'name' => 'Salman',
            'email' => 'salman@salman.com',
            'password' => Hash::make('test11')
        ]);
    }
}
