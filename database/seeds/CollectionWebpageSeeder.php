<?php

use Illuminate\Database\Seeder;

class CollectionWebpageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Code below assumes there are users that have multiple web pages saved

        // Create 2 collections per user
        // Get 2 webpages per user
        // Assign 1 webpage to each collection
        App\User::all()->each(function($user){
            App\UserWebpage::where('user_id', $user->id)->take(2)->get()->each(function($user_webpage) use ($user){
                // $collection = factory(App\Collection::class)->create(['user_id' => $user_webpage->user_id]);
                $collection = $user->collections()->first();
                $collection->userWebpages()->attach($user_webpage->id);
            });
        });
    }
}
