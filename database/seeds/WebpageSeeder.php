<?php

use Illuminate\Database\Seeder;

class WebpageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // factory(App\User::class, 4)->create();
      factory(App\Webpage::class, 10)->create();

      $webpages = App\Webpage::all();

      App\User::all()->each(function($user) use ($webpages) {
        $user->webpages()->attach(
          $webpages->random(2)->pluck('id')
        );
      });
    }
}
