<?php

use Faker\Generator as Faker;

$factory->define(App\Webpage::class, function (Faker $faker) {
    return [
        'url' => $faker->unique()->url,
        'title' => $faker->unique()->sentence(4)
    ];
});
