<?php

use Faker\Generator as Faker;

$factory->define(App\Collection::class, function (Faker $faker) {
    return [
      'title' => $faker->sentence(6),
      'description' => $faker->sentence(3),
      'user_id' => function(){
        return factory('App\User')->create()->id;
      }
    ];
});
