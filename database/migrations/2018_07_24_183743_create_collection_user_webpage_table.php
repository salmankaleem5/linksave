<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollectionUserWebpageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collection_user_webpage', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('collection_id');
            $table->unsignedInteger('user_webpage_id');

            $table->foreign('collection_id')
                  ->references('id')->on('collections')
                  ->onDelete('cascade');

            $table->foreign('user_webpage_id')
                  ->references('id')->on('user_webpage')
                  ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collection_user_webpage');
    }
}
