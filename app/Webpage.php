<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
Use App\UserWebpage;

class Webpage extends Model
{
  protected $fillable = ['url', 'title'];

  public function users(){
    return $this->belongsToMany('App\User')->using('App\UserWebpage')->withTimestamps();
  }

  public function userWebpages(){
    return $this->hasMany('App\UserWebpage', 'webpage_id');
  }
}
