<?php

namespace App\Services;

use App\User;
use App\Collection;
use App\Permission;
use App\Role;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

// TODO: in methods, check if user passes an instance of a model instead of an id. use model instances directly instead of having to re-query to find model instance
// review function naming
class CollectionPermissionsManager {
    private $table = 'collection_user_role';

    public function retrieveCollectionUsers( $collection_id ){
        if( $collection_id instanceof Collection ){
            $collection_id = $collection_id->getKey();
        }

        $results = DB::table( $this->table )
                    ->where( $this->table.'.collection_id', '=', $collection_id )
                    ->join('users', $this->table.'.user_id', '=', 'users.id')
                    ->select($this->table.'.role_id', 'users.id', 'users.name', 'users.email')
                    ->get();
        return $results;
    }

    public function newCollectionRole( $collection_id, $user_id, $role_id ){
        if( $collection_id instanceof Collection ){
            $collection_id = $collection_id->getKey();
        }
        if( $user_id instanceof User ){
            $user_id = $user_id->getKey();
        }
        if( $role_id instanceof Role ){
            $role_id = $role_id->getKey();
        }

        if( $this->getRole($collection_id, $user_id) ){
            return null;
        }

        $id = DB::table($this->table)->insertGetId([
            'user_id' => $user_id,
            'collection_id' => $collection_id,
            'role_id' => $role_id,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        return $id;
    }

    public function removeRole( $collection_id, $user_id ){
        if( $collection_id instanceof Collection ){
            $collection_id = $collection_id->getKey();
        }
        if( $user_id instanceof User ){
            $user_id = $user_id->getKey();
        }

        $rowsAffected = DB::table($this->table)->where([
            ['user_id', '=', $user_id],
            ['collection_id', '=', $collection_id]
        ])->delete();

        return $rowsAffected == 1;
    }

    public function updateRole( $collection_id, $user_id, $role_id ){
        if( $collection_id instanceof Collection ){
            $collection_id = $collection_id->getKey();
        }
        if( $user_id instanceof User ){
            $user_id = $user_id->getKey();
        }
        if( $role_id instanceof Role ){
            $role_id = $role_id->getKey();
        }

        $rowsAffected = DB::table($this->table)
            ->where([
                ['user_id', '=', $user_id],
                ['collection_id', '=', $collection_id],
            ])
            ->update([
                'role_id' => $role_id
            ]);

        return $rowsAffected == 1;
    }

    public function getRoleId( $collection_id, $user_id ){
        if( $collection_id instanceof Collection ){
            $collection_id = $collection_id->getKey();
        }
        if( $user_id instanceof User ){
            $user_id = $user_id->getKey();
        }
        $whereArgs = [
            ['user_id', '=', $user_id],
            ['collection_id', '=', $collection_id]
        ];

        $role_id = DB::table( $this->table )->where($whereArgs)->value('role_id');

        return $role_id;
    }

    public function getRole( $collection_id, $user_id ){
        if( $collection_id instanceof Collection ){
            $collection_id = $collection_id->getKey();
        }
        if( $user_id instanceof User ){
            $user_id = $user_id->getKey();
        }

        $role_id = $this->getRoleId( $collection_id, $user_id );

        if( !$role_id ){
            return null;
        }

        $role = Role::find( $role_id );

        return $role;
    }

    public function checkUserForRoleInCollection( $collection_id, $user_id, $role_id ){
        if( $collection_id instanceof Collection ){
            $collection_id = $collection_id->getKey();
        }
        if( $user_id instanceof User ){
            $user_id = $user_id->getKey();
        }
        if( $role_id instanceof Role ){
            $role_id = $role_id->getKey();
        }

        return DB::table($this->table)->where([
            ['user_id', '=', $user_id],
            ['collection_id', '=', $collection_id],
            ['role_id', '=', $role_id]
        ])->exists();
    }

    public function checkRoleForPermission( $role, $permission_type ){
        return $role->permissions()->where('permission_type', $permission_type)->exists();
    }

    public function isOwnerAttemptingAction( Collection $collection, User $user ){
        // Always authorize if collection owner is attempting action
        return $user->id === $collection->user->id;
    }

    public function isCollectionPublic( Collection $collection ){
        return $collection->is_public;
    }

    public function userHasCollectionPermission( Collection $collection, User $user, $permission_type ){

        if( $this->isOwnerAttemptingAction( $collection, $user ) ){
            return true;
        }

        if( $permission_type === 'view' && $this->isCollectionPublic( $collection ) ){
            return true;
        }

        $role = $this->getRole( $collection, $user );
        if( !$role ){
            return false;
        }

        return $this->checkRoleForPermission( $role, $permission_type );
    }
}
