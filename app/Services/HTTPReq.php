<?php

namespace App\Services;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;

class HTTPReq {
  protected $client;

  public function getPageTitle($url){
    $client = new Client();

    try {
      $response = $client->request('GET', $url);
      $body = (string) $response->getBody();

      $doc = new \DOMDocument();
      @$doc->loadHTML($body);
      $titleTagNodeList = $doc->getElementsByTagName('title');
      if( $titleTagNodeList->length <= 0 ){
        return false;
      }

      Log::debug('made request');

      return ($titleTagNodeList->item(0))->textContent;
    } catch( RequestException $e ){
      Log::debug('exception occured');
      report( Psr7\str($e->getRequest()) );
      if ($e->hasResponse()) {
        report( Psr7\str($e->getResponse()) );
      }
    } catch( ClientException $e ){
      Log::debug('exception occured');
      report( Psr7\str($e->getRequest()) );
      report( Psr7\str($e->getResponse()) );
    }
  }
}
