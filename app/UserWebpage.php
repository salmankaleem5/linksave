<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;
use App\User;
use App\Webpage;

class UserWebpage extends Pivot
{
    protected $with = ['webpage'];

    public function collections(){
        return $this->belongsToMany('App\Collection', 'collection_user_webpage')->using('App\CollectionUserWebpage')->withTimestamps();
    }

    public function webpage(){
        return $this->belongsTo('App\Webpage', 'webpage_id');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function scopeFor($query, $user_id, $webpage_id){
        if( $user_id instanceof User ){
            $user_id = $user_id->getKey();
        }
        if( $webpage_id instanceof Webpage ){
            $webpage_id = $webpage_id->getKey();
        }

        $query->where(compact('user_id', 'webpage_id'));
    }

    public function scopeFromUser($query, $user_id){
        if( $user_id instanceof User ){
            $user_id = $user_id->getKey();
        }

        $query->where(compact('user_id'));
    }
}
