<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;
use App\Collection;
use App\UserWebpage;

class CollectionUserWebpage extends Pivot
{
    public function scopeFor($query, $collection_id, $user_webpage_id){
        if( $collection_id instanceof Collection ){
            $collection_id = $collection_id->getKey();
        }
        if( $user_webpage_id instanceof UserWebpage ){
            $user_webpage_id = $user_webpage_id->getKey();
        }

        $query->where(compact('collection_id', 'user_webpage_id'));
    }

    public function userWebpage(){
        return $this->belongsTo('App\UserWebpage', 'user_webpage_id');
    }

    public function collection(){
        return $this->belongsTo('App\User', 'collection_id');
    }
}
