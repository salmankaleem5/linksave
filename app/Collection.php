<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    protected $fillable = ['title', 'description', 'user_id', 'is_public'];

    protected $with = ['user'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function userWebpages(){
        return $this->belongsToMany('App\UserWebpage', 'collection_user_webpage', 'collection_id', 'user_webpage_id')->using('App\CollectionUserWebpage')->withTimestamps();
    }

    public function path(){
        return "/collection/{$this->id}";
    }

    public function setIsPublicAttribute($value)
    {
        if( $value === true ){
            $this->attributes['is_public'] = 1;
        } else if( $value === false ){
            $this->attributes['is_public'] = 0;
        }
    }

    public function makePublic(){
        $this->is_public = true;
        $this->save();
    }

    public function makePrivate(){
        $this->is_public = false;
        $this->save();
    }

    public function getIsPublicAttribute($value){
        return boolval( $value );
    }
}
