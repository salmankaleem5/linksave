<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $guarded = [];

    public function permissions(){
        return $this->belongsToMany('App\Permission', 'role_permission', 'role_id', 'permission_id')->withTimestamps();
    }
}
