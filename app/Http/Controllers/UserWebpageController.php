<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserWebpage;
use App\User;
use App\Webpage;

class UserWebpageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        $this->authorize('create', UserWebpage::class);

        $validatedData = $request->validate([
            'url' => 'required|url',
            'title' => 'required|string'
        ]);

        // If webpage already exists in webpages table,
        // then retrieve
        $webpage = Webpage::where('url', $request->input('url'))->first();

        // Create webpage if it doesn't exist
        if( !$webpage ){
            $webpage = Webpage::create([
                'url' => $request->input('url'),
                'title' => $request->input('title')
            ]);
        }

        // create relationship with user and webpage
        if( $user->webpages()->where('url', $webpage->url)->doesntExist() ){
            $user->attachWebpage( $webpage );
        }

        $userWebpage = UserWebpage::for( $user->id, $webpage->id )->first();

        if( request()->wantsJson() ){
            return response()->json($userWebpage, 201);
        }

        return redirect( route('home') );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, User $user, UserWebpage $userWebpage)
    {
        $this->authorize('delete', $userWebpage);

        $user->detachWebpage( $userWebpage->webpage->id );

        if( request()->wantsJson() ){
            return response()->json([], 204);
        }

        return redirect( route('home') );
    }
}
