<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreCollectionUser;
use Exception;
use Illuminate\Support\Facades\Log;
use App\Services\CollectionPermissionsManager as PermissionsManager;

class CollectionUserController extends Controller
{
    protected $permManager;

    public function __construct(PermissionsManager $permManager)
    {
        $this->permManager = $permManager;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
    //     //
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCollectionUser $request, \App\Collection $collection)
    {
        $this->authorize( 'authorizeUser', $collection );

        $validatedData = $request->validated();

        $user = \App\User::byEmail( $validatedData['email'] )->first();
        $userId = $user->id;

        $addCollectionRole = $this->permManager->newCollectionRole( $collection->id, $userId, $validatedData['roleId'] );

        if( $addCollectionRole === null ){
            return response()->json([
                'message' => 'This user already has a role in this collection'
            ], 400);
        }

        return response()->json([
            'message' => 'User added to collection',
            'user' => [
                'email' => $user->email,
                'id' => $userId,
                'name' => $user->name,
                'role_id' => $validatedData['roleId']
            ]
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     //
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, \App\Collection $collection, \App\User $user)
    {
        $this->authorize( 'authorizeUser', $collection );
        // get new, validated role id
        // update user's role id in this collection
        $validatedData = $request->validate([
            'roleId' => 'bail|required|exists:roles,id'
        ]);

        $roleUpdated = $this->permManager->updateRole( $collection->id, $user->id, $validatedData['roleId'] );

        if( !$roleUpdated ){
            return response()->json([
                'message' => "Specified user doesn't have a role in this collection"
            ], 400);
        }

        return response()->json([], 204);
    }

    public function bulkUpdate(Request $request, \App\Collection $collection)
    {
        $this->authorize( 'authorizeUser', $collection );

        $validatedData = $request->validate([
            'usersToUpdate' => [
                'required',
                'array',
                function( $attribute, $value, $fail ){
                    $invalidUserIds = false;
                    $invalidRoleIds = false;
                    foreach( $value as $userId => $roleId ){
                        if( !\App\User::find($userId) ){
                            $invalidUserIds = true;
                        }
                        if( !\App\Role::find($roleId) ){
                            $invalidRoleIds = true;
                        }
                    }
                    if( $invalidUserIds && $invalidRoleIds ){
                        return $fail('Attempting to update invalid users and with roles that are not valid. Please contact the website administrator.');
                    } else if( $invalidUserIds && !$invalidRoleIds ){
                        return $fail('Attempting to update invalid users. Please contact the website administrator.');
                    } else if( $invalidRoleIds ) {
                        return $fail('Attempting to update with roles that are not valid. Please contact the website administrator.');
                    }
                }
            ]
        ]);

        // Results array: tracks status of each update attempt
        $results = [];
        foreach( $validatedData['usersToUpdate'] as $userId => $roleId ){
            $roleUpdated = $this->permManager->updateRole( $collection->id, $userId, $roleId );

            if( $roleUpdated ){
                $results[] = [
                    'id' => $userId,
                    'status' => 'success',
                    'roleId' => $roleId
                ];
            } else {
                $results[] = [
                    'id' => $userId,
                    'status' => 'failure',
                    'message' => "This user doesn't have a role in this collection"
                ];
            }
        }

        return response()->json($results, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(\App\Collection $collection, \App\User $user)
    {
        $this->authorize( 'authorizeUser', $collection );

        // better error handling
        $roleRemoved = $this->permManager->removeRole( $collection->id, $user->id );
        if( $roleRemoved ){
            return response()->json([], 204);
        } else {
            return response()->json(['message' => "This user doesn't have a role in this collection"], 400);
        }
    }

    public function bulkDestroy(Request $request, \App\Collection $collection)
    {
        $this->authorize( 'authorizeUser', $collection );
        // body has ids of users to remove from this collection

        $validatedData = $request->validate([
            'usersToRemove' => [
                'required',
                'array',
                function( $attribute, $value, $fail ){
                    foreach( $value as $userId ){
                        if( !\App\User::find($userId) ){
                            return $fail('User ID ' .$userId. ' is invalid');
                        }
                    }
                }
            ]
        ]);

        // Results array: tracks status of each delete attempt
        $results = [];
        foreach( $validatedData['usersToRemove'] as $k => $userId ){
            $roleRemoved = $this->permManager->removeRole( $collection->id, $userId );

            if( $roleRemoved ){
                $results[$k] = [
                    'id' => $userId,
                    'status' => 'success'
                ];
            } else {
                $results[$k] = [
                    'id' => $userId,
                    'status' => 'failure',
                    'message' => "This user doesn't have a role in this collection"
                ];
            }
        }

        return response()->json([$results], 200);
    }
}
