<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\HTTPReq;
use Illuminate\Support\Facades\Log;
use App\Webpage;

class WebpageDataController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    // Add form request validator
    public function get(Request $request)
    {
        $url = $request->query('url');

        // Check if web page already exists in db
        $webpage = Webpage::where('url', $request->input('url'))->first();

        if( $webpage ){
            Log::debug('test-get-title: already in db');
            return response()->json( $webpage->title );
        } else {
            Log::debug('test-get-title: need to make request');
            $httpReqService = new HTTPReq();
            return response()->json( $httpReqService->getPageTitle($url) );
        }
    }
}
