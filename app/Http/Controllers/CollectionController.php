<?php

namespace App\Http\Controllers;

use App\Collection;
use App\UserWebpage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\CollectionPermissionsManager;
use Validator;
use Exception;
use ReflectionClass;

class CollectionController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $collections = $user->collections;

        return $collections;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Collection::class);

        // Validate request
        // Create new collection
        $request->validate([
            'title' => 'required|string',
            'description' => 'nullable|string'
        ]);

        $collection = Collection::create([
            'user_id' => auth()->id(),
            'title' => request('title'),
            'description' => request('description')
        ]);

        return redirect( $collection->path() );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Collection  $collection
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Collection $collection)
    {
        $this->authorize('view', $collection);

        // Get all of user's webpages in this collection
        $collectionUserWebpages = $collection->userWebpages()->get();

        // For current user, get all of user's webpages not in this collection.
        $userWebpageIDsInCollection = $collectionUserWebpages->pluck('id');
        $userWebpagesNotInCollection = UserWebpage::fromUser( Auth::id() )->whereNotIn('id', $userWebpageIDsInCollection)->get();


        $collectionUsers = app('App\Services\CollectionPermissionsManager')->retrieveCollectionUsers( $collection->id );

        $availableRoles = \App\Role::all();

        return view('layouts.collections.show', compact('collection', 'collectionUserWebpages', 'userWebpagesNotInCollection', 'availableRoles', 'collectionUsers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Collection  $collection
     * @return \Illuminate\Http\Response
     */
    public function edit(Collection $collection)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Collection  $collection
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Collection $collection)
    {
        // @TODO: prevent duplicate roles for a user on 1 collection
        // @TODO: throw error if user tries to add self

        $validatedData = $request->validate([
             'isPublic' => 'required|boolean'
        ]);

        if( $validatedData['isPublic'] === true ){
            $collection->makePublic();
        } else {
            $collection->makePrivate();
        }

        return response()->json([], 204);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Collection  $collection
     * @return \Illuminate\Http\Response
     */
    public function destroy(Collection $collection)
    {
        $this->authorize('delete', $collection);

        $collection->delete();

        return redirect()->route('home')->with('status', "Collection deleted");
    }
}
