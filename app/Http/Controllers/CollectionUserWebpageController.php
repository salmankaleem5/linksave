<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Collection;
use App\UserWebpage;
use App\Webpage;
use Illuminate\Support\Facades\Auth;

class CollectionUserWebpageController extends Controller
{
    public function store(Request $request, Collection $collection){
        $this->authorize('update', $collection);

        $validatedData = $request->validate([
            'userWebpageId' => 'required|exists:user_webpage,id',
            'userId' => 'required|exists:users,id'
        ]);

        $userWebpage = UserWebpage::findOrFail( $request->userWebpageId );

        $collection->userWebpages()->attach( $userWebpage->id );

        if( request()->wantsJson() ){
            return response([], 201);
        }

        return redirect( $collection->path() );
    }

    public function destroy(Request $request, Collection $collection, UserWebpage $userWebpage){
        $this->authorize('update', $collection);

        $collection->userWebpages()->detach( $userWebpage->id );

        if( request()->wantsJson() ){
            return response([], 204);
        }

        return redirect( $collection->path() );
    }
}
