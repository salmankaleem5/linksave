<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreWebpage extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    // @TODO: Review this authorization
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    // @TODO: Review rules
    public function rules()
    {
        return [
            'url' => 'bail|required|url',
            'title' => 'bail|required|string'
        ];
    }
}
