<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCollectionUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'roleId' => 'bail|required|exists:roles,id',
            'email' => 'bail|required|email|exists:users,email'
        ];
    }

    public function messages()
    {
        return [
            'roleId.required' => 'A role ID is required',
            'roleId.exists' => 'The role must be a valid role from the dropdown above',
            'email.required' => 'An email address is required and must belong to a registered user',
            'email.exists' => 'The email address must belong to a registered user',
            'email.email' => 'The email address must be valid and belong to a registered user'
        ];
    }
}
