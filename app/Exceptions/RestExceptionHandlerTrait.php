<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Http\JsonResponse;
use ReflectionClass;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 *
 */
trait RestExceptionHandlerTrait
{
    /**
     * Controllers that should have their exceptions modified
     * @var Array
     */
    protected $controllers = [
        'App\Http\Controllers\\CollectionUserController'
    ];

    /**
     * Returns name of controller that resolved $request
     * @param  Illuminate\Http\Request $request
     * @return string          Name of controller that resolved $request
     */
    protected function getRouteControllerClass( $request )
    {
        $class = new ReflectionClass( $request->route()->controller );
        return $class->name;
    }

    /**
     * @todo : Need better name
     * Returns true if the controller that resolves the request
     * is in $controllers.
     * @return boolean
     */
    protected function isASpecifiedController( $request )
    {
        $controllerName = $this->getRouteControllerClass( $request );

        return array_search($controllerName, $this->controllers) !== FALSE;
    }

    // @TODO: current form is temporary. generalize this
    /**
     * Returns true if the given exception's response should be modified
     * @param  Exception $e [description]
     * @return boolean      [description]
     */
    protected function isASpecifiedException( Exception $e )
    {
        return $e instanceof ModelNotFoundException;
    }

    /**
     * Returns true if the given request and exception should be handled
     * by this trait
     * @param  Request $request
     * @param  Exception $e
     * @return boolean
     */
    protected function shouldHandleException(Request $request, Exception $e)
    {
        return $this->isASpecifiedException( $e ) && $request->expectsJson() && $this->isASpecifiedController($request);
    }

    /**
     * Generates custom response message for ModelNotFoundException
     * @param  Exception $e [description]
     * @return string       [description]
     */
    protected function generateModelNotFoundMessage( Exception $e )
    {
        // Remove namespace from model name
        $namespace = "App\\";
        return sprintf("%s not found. Please contact the site administrator.", substr($e->getModel(), strlen($namespace)));
    }

    /**
     * Returns modified response for an exception
     * @param  \Illuminate\Http\Request    $request
     * @param  \Exception $e
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getResponseForException( $request, Exception $e )
    {
        if (method_exists($e, 'render') && $response = $e->render($request)) {
            return Router::toResponse($request, $response);
        } elseif ($e instanceof Responsable) {
            return $e->toResponse($request);
        }

        $message = $e->getMessage();
        $id = null;
        if( $e instanceof ModelNotFoundException ){
            $id = 'model_not_found';
            $message = $this->generateModelNotFoundMessage($e);

            // Returns a NotFoundHttpException
            $e = parent::prepareException($e);
        }

        $response = parent::prepareJsonResponse($request, $e);
        $responseData = $response->getData(true);
        $newResponseData = array_merge($responseData, [
            'id' => $id,
            'message' => $message
        ]);
        $response->setData($newResponseData);

        return $response;

    }
}
