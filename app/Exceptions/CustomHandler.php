<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\Exceptions\RestExceptionHandlerTrait;

class CustomHandler extends Handler
{
    use RestExceptionHandlerTrait;
    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        // if application is in production mode, set up logging and log each exception, not generalized version of the exception
        // if request belongs to one of N specified controllers and expects json
        // if exception is a client error (4xx status code)
        // if $exception is an instance of any of N specified exceptions
        // then return specified general exception

        if( $this->shouldHandleException( $request, $exception ) ){
            return $this->getResponseForException( $request, $exception );
        } else {
            return response()->json("don't handle {$exception->getMessage()}", 200);
        }

        return parent::render($request, $exception);
    }
}
