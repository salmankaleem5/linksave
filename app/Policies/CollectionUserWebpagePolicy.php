<?php

namespace App\Policies;

use App\User;
use App\CollectionUserWebpage;
use Illuminate\Auth\Access\HandlesAuthorization;

class CollectionUserWebpagePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the collection user webpage.
     *
     * @param  \App\User  $user
     * @param  \App\CollectionUserWebpage  $collectionUserWebpage
     * @return mixed
     */
    public function view(User $user, CollectionUserWebpage $collectionUserWebpage)
    {
        //
    }

    /**
     * Determine whether the user can create collection user webpages.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the collection user webpage.
     *
     * @param  \App\User  $user
     * @param  \App\CollectionUserWebpage  $collectionUserWebpage
     * @return mixed
     */
    public function update(User $user, CollectionUserWebpage $collectionUserWebpage)
    {
        //
    }

    /**
     * Determine whether the user can delete the collection user webpage.
     *
     * @param  \App\User  $user
     * @param  \App\CollectionUserWebpage  $collectionUserWebpage
     * @return mixed
     */
    public function delete(User $user, CollectionUserWebpage $collectionUserWebpage)
    {
        //
    }
}
