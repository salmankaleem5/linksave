<?php

namespace App\Policies;

use App\User;
use App\Collection;
use Illuminate\Auth\Access\HandlesAuthorization;

class CollectionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the collection.
     *
     * @param  \App\User  $user
     * @param  \App\Collection  $collection
     * @return mixed
     */
    public function view(User $user, Collection $collection)
    {
        $collectionPermissionsManager = resolve('App\Services\CollectionPermissionsManager');

        $userHasViewPermission = $collectionPermissionsManager->userHasCollectionPermission( $collection, $user, 'view' );

        return $userHasViewPermission;
    }

    /**
     * Determine whether the user can create collections.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the collection.
     *
     * @param  \App\User  $user
     * @param  \App\Collection  $collection
     * @return mixed
     */
    public function update(User $user, Collection $collection)
    {
        $collectionPermissionsManager = resolve('App\Services\CollectionPermissionsManager');

        $userHasUpdatePermission = $collectionPermissionsManager->userHasCollectionPermission( $collection, $user, 'edit' );

        return $userHasUpdatePermission;
        // return $user->id === $collection->user->id;
    }

    /**
     * Determine whether the user can delete the collection.
     *
     * @param  \App\User  $user
     * @param  \App\Collection  $collection
     * @return mixed
     */
    public function delete(User $user, Collection $collection)
    {
        return $user->id === $collection->user->id;
    }

    /**
     * Determine whether the user can authorize other users to use this collection
     * @param  \App\User       $user
     * @param  \App\Collection $collection
     * @return mixed
     */
    public function authorizeUser(User $user, Collection $collection)
    {
        return $user->id === $collection->user->id;
    }
}
