<?php

namespace App\Policies;

use App\User;
use App\Webpage;
use Illuminate\Auth\Access\HandlesAuthorization;

class WebpagePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the webpage.
     *
     * @param  \App\User  $user
     * @param  \App\Webpage  $webpage
     * @return mixed
     */
    public function view(User $user, Webpage $webpage)
    {
        //
    }

    /**
     * Determine whether the user can create webpages.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the webpage.
     *
     * @param  \App\User  $user
     * @param  \App\Webpage  $webpage
     * @return mixed
     */
    public function update(User $user, Webpage $webpage)
    {
        //
    }

    /**
     * Determine whether the user can delete the webpage.
     *
     * @param  \App\User  $user
     * @param  \App\Webpage  $webpage
     * @return mixed
     */
    public function delete(User $user, Webpage $webpage)
    {
        //
    }
}
