<?php

namespace App\Policies;

use App\User;
use App\UserWebpage;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserWebpagePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the user webpage.
     *
     * @param  \App\User  $user
     * @param  \App\UserWebpage  $userWebpage
     * @return mixed
     */
    public function view(User $user, UserWebpage $userWebpage)
    {
        //
    }

    /**
     * Determine whether the user can create user webpages.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the user webpage.
     *
     * @param  \App\User  $user
     * @param  \App\UserWebpage  $userWebpage
     * @return mixed
     */
    public function update(User $user, UserWebpage $userWebpage)
    {
        //
    }

    /**
     * Determine whether the user can delete the user webpage.
     *
     * @param  \App\User  $user
     * @param  \App\UserWebpage  $userWebpage
     * @return mixed
     */
    public function delete(User $user, UserWebpage $userWebpage)
    {
        return $user->id === $userWebpage->user->id;
    }
}
