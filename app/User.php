<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Webpage;
use App\UserWebpage;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function webpages(){
        return $this->belongsToMany('App\Webpage')->using('App\UserWebpage')->withTimestamps();
    }

    public function userWebpages(){
        return UserWebpage::fromUser( $this->id )->get();
    }

    public function addWebpage(Webpage $webpage){
        $this->webpages()->save($webpage);
    }

    public function attachWebpage(Webpage $webpage){
        $this->webpages()->attach( $webpage->id );
    }

    public function detachWebpage($webpage_id){
        if( $webpage_id instanceof Webpage ){
            $webpage_id = $webpage_id->getKey();
        }
        $this->webpages()->detach( $webpage_id );
    }

    public function collections(){
        return $this->hasMany('App\Collection');
    }

    public function scopeByEmail($query, $email){
        $query->where(compact('email'));
    }
}
