<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Collection' => 'App\Policies\CollectionPolicy',
        'App\CollectionUserWebpage' => 'App\Policies\CollectionUserWebpagePolicy',
        'App\UserWebpage' => 'App\Policies\UserWebpagePolicy',
        'App\Webpage' => 'App\Policies\WebpagePolicy'
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
