<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\CollectionPermissionsManager;

class CollectionPermissionsProvider extends ServiceProvider
{
    protected $defer = true;
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('CollectionPermissionsManager', function($app){
            return new CollectionPermissionsManager();
        });
    }
}
