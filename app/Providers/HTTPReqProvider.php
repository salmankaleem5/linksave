<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\HTTPReq;

class HTTPReqProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(HTTPReq::class, function($app){
            return new HTTPReq();
        });
    }
}
